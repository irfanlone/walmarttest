
//
//  helper.swift
//  WalmartTest
//
//  Created by Irfan on 11/28/17.
//  Copyright © 2017 Eden. All rights reserved.
//

import Foundation


enum SerializationError: Error {
  case missing(String)
  case invalid(String, Any)
}
