//
//  RouteCell.swift
//  WalmartTest
//
//  Created by Irfan on 11/29/17.
//  Copyright © 2017 Eden. All rights reserved.
//

import UIKit

class RouteCell: UITableViewCell {
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var busImageView: UIImageView!
  
}
