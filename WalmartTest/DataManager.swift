//
//  DataManager.swift
//  WalmartTest
//
//  Created by Irfan on 11/28/17.
//  Copyright © 2017 Eden. All rights reserved.
//

import Foundation

class DataManager {
  
  public class func loadDataFrom(url: URL, completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
    let loadDataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
      if let _ = error {
        completion(nil, error)
      } else if let response = response as? HTTPURLResponse {
        if response.statusCode != 200 {
          let statusError = NSError(domain: "walmart.test",
                                    code: response.statusCode,
                                    userInfo: [NSLocalizedDescriptionKey: "HTTP status code has unexpected value."])
          completion(nil, statusError)
        } else {
          completion(data, nil)
        }
      }
    }
    loadDataTask.resume()
  }
  
}
