//
//  RouteViewController.swift
//  WalmartTest
//
//  Created by Irfan on 11/28/17.
//  Copyright © 2017 Eden. All rights reserved.
//

import UIKit

class RouteViewController: UIPageViewController, UIPageViewControllerDataSource {
  
  var data : [BusRoute]!
  var selectedRouteIndex : Int!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.dataSource = self
    self.setViewControllers([getViewControllerAtIndex(selectedRouteIndex ?? 0)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    self.title = "Route"
    self.automaticallyAdjustsScrollViewInsets = false
  }
  
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    let pageContent = viewController as! RouteContentViewController
    var index = pageContent.pageIndex
    if ((index == 0) || (index == NSNotFound)) {
      return nil
    }
    index -= 1
    return getViewControllerAtIndex(index)
  }
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    let pageContent = viewController as! RouteContentViewController
    var index = pageContent.pageIndex
    if (index == NSNotFound) {
      return nil
    }
    index += 1
    if (index == data.count) {
      return nil;
    }
    return getViewControllerAtIndex(index)
  }
  
  func getViewControllerAtIndex(_ index: Int) -> RouteContentViewController {
    let storyboard = UIStoryboard(name: "Route", bundle: nil)
    let pageContentViewController = storyboard.instantiateViewController(withIdentifier: "RouteContentViewController") as! RouteContentViewController
    pageContentViewController.route = data[index]
    pageContentViewController.pageIndex = index
    return pageContentViewController
  }
  
  
}
