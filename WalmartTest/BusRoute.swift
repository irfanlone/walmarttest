//
//  BusRoute.swift
//  WalmartTest
//
//  Created by Irfan on 11/28/17.
//  Copyright © 2017 Eden. All rights reserved.
//

import Foundation


struct BusRoute {
  let id: String
  let accessible: Bool
  let imageUrl: String?
  let name: String
  let description: String
  let stops: [BusStop]
}


extension BusRoute {
  init(json: [String: Any]) throws {
    guard let id = json["id"] as? String else {
      throw SerializationError.missing("id")
    }
    guard let accessible = json["accessible"] as? Bool else {
      throw SerializationError.missing("accessible")
    }
    guard let name = json["name"] as? String else {
      throw SerializationError.missing("name")
    }
    guard let description = json["description"] as? String else {
      throw SerializationError.missing("description")
    }
    let imageUrl = json["imageUrl"] as? String

    guard let stops = json["stops"] as? [[String: Any]] else {
      throw SerializationError.missing("stops")
    }
    
    var busStops = [BusStop]()
    for case let stop in stops {
      if let stop = BusStop(json: stop as! [String : String]) {
        busStops.append(stop)
      }
    }
    
    self.id = id
    self.accessible = accessible
    self.imageUrl = imageUrl
    self.name = name
    self.description = description
    self.stops = busStops
  }

}
