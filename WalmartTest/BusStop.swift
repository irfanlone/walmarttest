//
//  BusStop.swift
//  WalmartTest
//
//  Created by Irfan on 11/28/17.
//  Copyright © 2017 Eden. All rights reserved.
//

import Foundation

struct BusStop {
  let name: String
}

extension BusStop {
  init?(json: [String: String]) {
    guard let name = json["name"] else {
      return nil
    }
    
    self.name = name
  }

}
