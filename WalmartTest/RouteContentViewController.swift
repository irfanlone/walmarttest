//
//  RouteContentViewController.swift
//  WalmartTest
//
//  Created by Irfan on 11/28/17.
//  Copyright © 2017 Eden. All rights reserved.
//

import UIKit

class RouteContentViewController: UIViewController {
  
  @IBOutlet weak var routeNameLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var busImageView: UIImageView!
  @IBOutlet weak var accessibleImageView: UIImageView!
  
  @IBOutlet weak var scrollView: UIScrollView!
  
  var pageIndex: Int = 0
  var route: BusRoute!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    routeNameLabel.text = route.name
    descriptionLabel.text = route.description
    if route.accessible == true {
      let image = UIImage(named: "accessibility_icon")
      accessibleImageView.image = image
      accessibleImageView.clipsToBounds = true
      accessibleImageView.layer.masksToBounds = true
    }
    
    loadBusImage()
    
    renderAllStops()
  }
  
  func loadBusImage() {
    if let imageUrl = route.imageUrl {
      let url = URL(string: imageUrl)
      DataManager.loadDataFrom(url: url!) { (data, err) in
        if let _ = err {
          return
        }
        if let data = data {
          DispatchQueue.main.async {
            self.busImageView?.image = UIImage(data: data)
          }
        }
      }
    }
  }
  
  func renderAllStops() {
    let stackView = UIStackView()
    stackView.translatesAutoresizingMaskIntoConstraints = false
    stackView.axis = UILayoutConstraintAxis.vertical
    stackView.alignment = UIStackViewAlignment.center
    stackView.spacing = 20
    stackView.distribution = .fillEqually
    
    for (index, stop) in route.stops.enumerated() {
      
      let textLabel = UILabel()
      textLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
      textLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
      textLabel.text = stop.name
      textLabel.textAlignment = .left
      textLabel.translatesAutoresizingMaskIntoConstraints = false
      
      let dotImageView = UIImageView()
      dotImageView.translatesAutoresizingMaskIntoConstraints = false
      dotImageView.contentMode = .scaleAspectFit
      dotImageView.image = UIImage(named: "dot_icon.png")
      
      let myview = UIView()
      myview.translatesAutoresizingMaskIntoConstraints = false
      myview.heightAnchor.constraint(equalToConstant: 40).isActive = true
      myview.addSubview(dotImageView)
      myview.addSubview(textLabel)
      
      dotImageView.leftAnchor.constraint(equalTo: myview.leftAnchor, constant: 20).isActive = true
      dotImageView.topAnchor.constraint(equalTo: myview.topAnchor, constant: -20).isActive = true
      textLabel.rightAnchor.constraint(equalTo: myview.rightAnchor).isActive = true
      textLabel.topAnchor.constraint(equalTo: myview.topAnchor, constant: 0).isActive = true
      textLabel.leftAnchor.constraint(equalTo: dotImageView.rightAnchor, constant: 15).isActive = true
      
      if index != route.stops.count - 1 {
        let lineImageView = UIImageView()
        lineImageView.translatesAutoresizingMaskIntoConstraints = false
        lineImageView.contentMode = .scaleAspectFit
        lineImageView.image = UIImage(named: "line_icon.png")
        myview.addSubview(lineImageView)
        lineImageView.topAnchor.constraint(equalTo: dotImageView.bottomAnchor, constant: -25).isActive = true
        lineImageView.leftAnchor.constraint(equalTo: myview.leftAnchor, constant: 32).isActive = true
      }
      
      stackView.addArrangedSubview(myview)
    }
    
    self.scrollView.addSubview(stackView)
    stackView.topAnchor.constraint(equalTo: self.busImageView.bottomAnchor, constant: 30).isActive = true
  }

}
