//
//  ViewController.swift
//  WalmartTest
//
//  Created by Irfan on 11/28/17.
//  Copyright © 2017 Eden. All rights reserved.
//

import UIKit

let RouteCellReuseIdentifier = "RouteCellReuseIdentifier"
let urlString = "http://www.mocky.io/v2/5a0b474a3200004e08e963e5"

class MainViewController: UIViewController {
  
  @IBOutlet weak var tableview: UITableView!

  var busRoutes : [BusRoute] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableview.dataSource = self
    tableview.delegate = self
    
    self.title = "Routes"
    loadData()
    //TODO: show a loading indicator while the data is laoding
  }
  
  func loadData() {
    let url = URL(string: urlString)
    DataManager.loadDataFrom(url: url!) { (data, err) in
      if let _ = err {
        return
      }
      if let data = data {
        guard let json = try? JSONSerialization.jsonObject(with: data) as? [Any] else {
          return
        }
        for case let result in json! {
          if let route = try? BusRoute(json: result as! [String : Any]) {
            self.busRoutes.append(route)
          }
        }
        DispatchQueue.main.async { [weak self] in
          self?.tableview.reloadData()
        }
      }
    }
    
  }
  
}


extension MainViewController : UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let storyboard = UIStoryboard(name: "Route", bundle: nil)
    let rvc = storyboard.instantiateViewController(withIdentifier: "RouteViewController") as! RouteViewController
    rvc.data = busRoutes
    rvc.selectedRouteIndex = indexPath.row
    self.navigationController?.pushViewController(rvc, animated: true)
    tableview.deselectRow(at: indexPath, animated: true)
  }
  
}


extension MainViewController : UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return busRoutes.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: RouteCellReuseIdentifier) as! RouteCell
    cell.tag = indexPath.row
    cell.name?.text = busRoutes[indexPath.row].name
    if let imageUrl = busRoutes[indexPath.row].imageUrl {
      let url = URL(string: imageUrl)
      DataManager.loadDataFrom(url: url!) { (data, err) in
        if let _ = err {
          return
        }
        if let data = data {
          DispatchQueue.main.async {
            if cell.tag == indexPath.row {
              cell.busImageView?.image = UIImage(data: data)
            }
          }
        }
      }
    }
    return cell
  }
  
}













